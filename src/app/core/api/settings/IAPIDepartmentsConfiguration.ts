/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which models the configuration object of all departments assignable to a statement.
 */
export interface IAPIDepartmentsConfiguration {

    /**
     * Object which contains all available department groups.
     */
    allDepartments: IAPIDepartmentGroups;

    /**
     * Object which contains the preselection of the department groups.
     */
    suggestedDepartments: IAPIDepartmentGroups;

}

/**
 * Interface which models a set of department groups.
 * Each key represents a group of departments and references a string array with the names of the
 * departments contained in the group. The values in such an array are unique per group, but not
 * necessarily unique in the overall structure.
 */
export interface IAPIDepartmentGroups {

    [groupName: string]: string[];

}

/**
 * Interface which models the underlying table to assign city and district values to sectors and departments.
 * Each key represents a pair of city and district concatenated with a #.
 */
export interface IAPIDepartmentTable {
    [cityDistrict: string]: {
        locationDesignation?: string;
        provides: string[];
        departments: IAPIDepartmentGroups;
    };
}
