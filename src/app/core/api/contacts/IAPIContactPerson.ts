/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents the model of all basic information of a specific contact.
 */
export interface IAPIContactPerson {
    companyId: string;
    companyName: string;
    email: string;
    id: string;
    firstName: string;
    lastName: string;
}
