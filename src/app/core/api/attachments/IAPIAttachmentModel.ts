/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents the model of an uploaded attachment in the back end data base.
 */
export interface IAPIAttachmentModel {

    /**
     * Unique ID of a specifcic attachment.
     */
    id: number;

    /**
     * Name which is used for display in the app.
     */
    name: string;

    /**
     * Type of the attachment, e.g. a PDF or text file.
     */
    type: string;

    /**
     * Size of a specific attachment in bytes.
     */
    size: number;

    /**
     * Timestamp file.
     */
    timestamp: string;

    /**
     * List of IDs for tagging.
     */
    tagIds: string[];
}
