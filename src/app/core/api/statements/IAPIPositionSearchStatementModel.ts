/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface that represents the model of a statement from a position search call. Similiar to IAPIStatementModel but has information
 * specifically for the position search such as position and dueDate.
 */
export interface IAPIPositionSearchStatementModel {

    id: number;
    title: string;
    position: string;
    typeId: string;
    dueDate: string;
    finished: boolean;

}
