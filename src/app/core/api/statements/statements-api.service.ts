/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {objectToHttpParams, urlJoin} from "../../../util";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIDepartmentGroups} from "../settings";
import {IAPIPaginationResponse, IAPIPositionSearchOptions, IAPISearchOptions} from "../shared";
import {IAPICommentModel} from "./IAPICommentModel";
import {IAPIDashboardStatementModel} from "./IAPIDashboardStatementModel";
import {IAPIPositionSearchStatementModel} from "./IAPIPositionSearchStatementModel";
import {IAPISectorsModel} from "./IAPISectorsModel";
import {IAPIPartialStatementModel, IAPIStatementModel} from "./IAPIStatementModel";
import {IAPITextblockHistoryModel} from "./IAPITextblockHistoryModel";
import {IAPIWorkflowData} from "./IAPIWorkflowData";

@Injectable({
    providedIn: "root"
})
export class StatementsApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches a list of existing statements from the back end data base.
     * @param id IDs of statements to fetch.
     */
    public getStatements(...id: number[]) {
        const endPoint = "statements";
        const params = {id: id.map((_) => "" + _)};
        return this.httpClient.get<IAPIStatementModel[]>(urlJoin(this.baseUrl, endPoint), {params});
    }

    /**
     * Search for a paginated list of statements in the back end data base.
     */
    public getStatementSearch(searchOptions: IAPISearchOptions) {
        const endPoint = "statementsearch";
        const params = objectToHttpParams({...searchOptions});
        return this.httpClient.get<IAPIPaginationResponse<IAPIStatementModel>>(urlJoin(this.baseUrl, endPoint), {params});
    }

    /**
     * Search for a list of statements in the back end data base.
     */
    public getStatementPositionsSearch(searchOptions: IAPIPositionSearchOptions) {
        const endPoint = "statementpositionsearch";
        const params = objectToHttpParams({...searchOptions});
        return this.httpClient.get<IAPIPositionSearchStatementModel[]>(urlJoin(this.baseUrl, endPoint), {params});
    }

    /**
     * Fetches all basic details for a specific statement.
     * @param id Id of the statement to fetch.
     */
    public getStatement(id: number) {
        const endPoint = `statements/${id}`;
        return this.httpClient.get<IAPIStatementModel>(urlJoin(this.baseUrl, endPoint));
    }

    public getTextblockHistory(id: number) {
        const endPoint = `statements/${id}/history`;
        return this.httpClient.get<IAPITextblockHistoryModel>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Creates a new statement in the back end data base.
     */
    public putStatement(statement: IAPIPartialStatementModel) {
        const endPoint = "statements";
        return this.httpClient.post<IAPIStatementModel>(urlJoin(this.baseUrl, endPoint), statement);
    }

    /**
     * Creates a new statement in the back end data base.
     */
    public postStatement(statementId: number, taskId: string, statement: IAPIPartialStatementModel) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/statement`;
        return this.httpClient.post<IAPIStatementModel>(urlJoin(this.baseUrl, endPoint), statement);
    }

    /**
     * Fetches the workflow information of a specific statement.
     */
    public getWorkflowData(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflow`;
        return this.httpClient.get<IAPIWorkflowData>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Creates or updates the workflow data in the back end data base.
     */
    public postWorkflowData(statementId: number, taskId: string, body: IAPIWorkflowData) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/workflow`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), body);
    }

    /**
     * Fetches the current status of the departments that have finished their contribution to the statement..
     */
    public getContributions(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflow/contributions`;
        return this.httpClient.get<IAPIDepartmentGroups>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Creates or updates the contributions in the back end data base.
     */
    public postContributions(statementId: number, taskId: string, body: IAPIDepartmentGroups) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/workflow/contributions`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), body);
    }

    /**
     * Fetches the IDs of all parents to a specific statement.
     */
    public getParentIds(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflow/parents`;
        return this.httpClient.get<number[]>(urlJoin(this.baseUrl, endPoint));
    }


    /**
     * Fetches the IDs of all children of a specific statement.
     */
    public getChildrenIds(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflow/children`;
        return this.httpClient.get<number[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Updates the IDs of all parents to specific statement in the back end data base.
     */
    public postParentIds(statementId: number, taskId: string, parentIds: number[]) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/workflow/parents`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), parentIds);
    }

    /**
     * Returns a list of all comments for a specific statement.
     */
    public getComments(statementId: number) {
        const endPoint = `/process/statements/${statementId}/comments`;
        return this.httpClient.get<IAPICommentModel[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Creates a component in the back end data base for a specific statement.
     */
    public putComment(statementId: number, comment: string) {
        const endPoint = `/process/statements/${statementId}/comments`;
        return this.httpClient.put(urlJoin(this.baseUrl, endPoint), comment);
    }

    /**
     * Deletes a comment in the back end data base for a specific statement.
     */
    public deleteComment(statementId: number, commentId: number) {
        const endPoint = `/process/statements/${statementId}/comments/${commentId}`;
        return this.httpClient.delete<void>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Edits a comment in the back end data base for a specific statement with a new text.
     */
    public editComment(statementId: number, commentId: number, newText: string) {
        const endPoint = `/process/statements/${statementId}/comments/${commentId}`;
        return this.httpClient.post<void>(urlJoin(this.baseUrl, endPoint), newText);
    }

    /**
     * Fetches the list of all sectors.
     */
    public getSectors(statementId: number) {
        const endPoint = `/statements/${statementId}/sectors`;
        return this.httpClient.get<IAPISectorsModel>(urlJoin(this.baseUrl, endPoint));
    }


    /**
     * Updates the contribution status of the user's department to true for the given statement.
     */
    public contribute(statementId: number, taskId: string) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/workflow/contribute`;
        return this.httpClient.patch(urlJoin(this.baseUrl, endPoint), null);
    }

    /**
     * Fetches the list of statements to display in the dashboard.
     */
    public getDashboardStatements() {
        const endPoint = "/dashboard/statements";
        return this.httpClient.get<IAPIDashboardStatementModel[]>(urlJoin(this.baseUrl, endPoint));
    }

}
