/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIDepartmentGroups} from "../settings";

/**
 * Interface which represents the workflow information for a specific statement in the back end data base.
 */
export interface IAPIWorkflowData {

    /**
     * Object which contains all mandatory departments assigned to a statement.
     */
    mandatoryDepartments: IAPIDepartmentGroups;

    /**
     * Object which contains all optional departments assigned to a statement.
     */
    optionalDepartments: IAPIDepartmentGroups;

    /**
     * String which represents the geographic position assigned to a statement.
     */
    geoPosition: string;

}
