/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents an entry in the text block arrangement.
 */
export interface IAPITextArrangementItemModel {

    /**
     * Defines the type of the arrangement entry.
     */
    type: TAPITextArrangementItemType;

    /**
     * ID of the text block template in the text block configuration (only of type "block").
     */
    textblockId?: string;

    /**
     * Each key found in the text block string will be replaced with its value (only of type "block").
     */
    placeholderValues: { [key: string]: string };

    /**
     * Adds automatically a new line after the entry.
     */
    addNewline?: boolean;

    /**
     * Replaces the whole entry with the given string.
     */
    replacement?: string;

}

export type TAPITextArrangementItemType = "block" | "text" | "newline" | "pagebreak";
