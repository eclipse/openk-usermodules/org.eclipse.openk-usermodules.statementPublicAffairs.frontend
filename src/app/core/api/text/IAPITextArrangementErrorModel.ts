/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIRequireRuleModel} from "./IAPIRequireRuleModel";

/**
 * Interface which represents an error in a text block arrangement.
 */
export interface IAPITextArrangementErrorModel {

    /**
     * Index of the erroneous item in the text block arrangement array.
     */
    arrangementId: 0;

    /**
     * Text block ID of the item.
     */
    textblockId: string;

    /**
     * Text block group to which the item belongs.
     */
    textblockGroup: string;

    /**
     * List of all unset placeholder variables.
     */
    missingVariables?: string[];

    /**
     * List of all unmet require rules.
     */
    requires?: IAPIRequireRuleModel[];

    /**
     * List of all excluded text block IDs which are currently in the arrangement.
     */
    excludes?: string[];

    /**
     * ID after which the current text block must be placed in the arrangement.
     */
    after?: string;

}
