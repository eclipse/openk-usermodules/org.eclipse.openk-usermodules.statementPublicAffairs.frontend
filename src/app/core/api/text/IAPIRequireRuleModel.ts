/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents a rule of required text blocks in an arrangement.
 */
export interface IAPIRequireRuleModel {

    /**
     * List of required text block IDs in the arrangement.
     */
    ids: string[];

    /**
     * If "and", all given IDs must be included in the arrangement.
     * If "xor", one but no other text block must be included in the arrangement.
     * If "or", at least one text block must be included in the arrangement.
     */
    type: TAPIRequireRuleType;

}

export type TAPIRequireRuleType = "and" | "xor" | "or";
