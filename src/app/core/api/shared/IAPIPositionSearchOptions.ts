/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents the options for a paginated search in the back end data base.
 */
export interface IAPIPositionSearchOptions {

    /**
     * Key to filter the search by type. Only show results of chosen type.
     */
    typeId?: number;

    /**
     * Key for filtering by due date.
     */
    dueDateFrom?: string;

    /**
     * Key for filtering by due date.
     */
    dueDateTo?: string;

}

