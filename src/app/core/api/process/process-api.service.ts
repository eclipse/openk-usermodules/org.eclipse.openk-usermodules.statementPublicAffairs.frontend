/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {urlJoin} from "../../../util";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIClaimDetails} from "./IAPIClaimDetails";
import {IAPIProcessObject} from "./IAPIProcessObject";
import {IAPIProcessTask} from "./IAPIProcessTask";
import {IAPIStatementHistory} from "./IAPIStatementHistory";

@Injectable({
    providedIn: "root"
})
export class ProcessApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches all current workflow tasks for a statement
     */
    public getStatementTasks(statementId: number) {
        const endPoint = `/process/statements/${statementId}/task`;
        return this.httpClient.get<IAPIProcessTask[]>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Claim an open task in the process of a statement
     * @param statementId Id of statement to which the task belongs
     * @param taskId Id of task
     */
    public claimStatementTask(statementId: number, taskId: string) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/claim`;
        return this.httpClient.post<IAPIProcessTask>(urlJoin(this.baseUrl, endPoint), undefined);
    }

    /**
     * Unclaim an open task in the process of a statement
     * @param statementId Id of statement to which the task belongs
     * @param taskId Id of task
     */
    public unclaimStatementTask(statementId: number, taskId: string) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/unclaim`;
        return this.httpClient.post<IAPIProcessTask>(urlJoin(this.baseUrl, endPoint), undefined);
    }

    /**
     * Gets the claim details for the specified task to check how long the task will remain claimed
     * @param statementId Id of statement to which the task belongs
     * @param taskId Id of task
     */
    public getClaimDetails(statementId: number, taskId: string) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/claim-details`;
        return this.httpClient.get<IAPIClaimDetails>(urlJoin(this.baseUrl, endPoint));
    }

    public cancelStatement(statementId: number) {
        const endPoint = `/process/statements/${statementId}/cancel`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), undefined);
    }

    /**
     * Complete an open task in the process of a statement
     * @param statementId Id of statement to which the task belongs
     * @param taskId Id of task
     * @param body Required information to complete the task
     */
    public completeStatementTask(statementId: number, taskId: string, body: IAPIProcessObject) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/complete`;
        return this.httpClient.post<void>(urlJoin(this.baseUrl, endPoint), body);
    }

    /**
     * Fetches the statement history.
     */
    public getStatementHistory(statementId: number) {
        const endPoint = `/process/statements/${statementId}/history`;
        return this.httpClient.get<IAPIStatementHistory>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches the statement process diagram. Response is a xml sent as text.
     */
    public getStatementProcessDiagram(statementId: number) {
        const endPoint = `/process/statements/${statementId}/workflowmodel`;
        return this.httpClient.get(urlJoin(this.baseUrl, endPoint), {responseType: "text"});
    }

    /**
     * Re-sends the outgoing email for a statement.
     */
    public dispatchStatement(statementId: number, taskId: string) {
        const endPoint = `/process/statements/${statementId}/task/${taskId}/mailandcomplete`;
        return this.httpClient.post(urlJoin(this.baseUrl, endPoint), null);
    }

}
