/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Data interface for the claim details for a task
 */
export interface IAPIClaimDetails {

    statementId: number;

    taskId: string;

    current: boolean;

    assignee: string;

    claimedUntilTime: string;

    currentTime: string;

}
