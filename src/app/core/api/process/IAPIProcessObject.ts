/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export interface IAPIProcessObject {

    [key: string]: IAPIProcessObject
    | { type: "Boolean"; value: boolean }
    | { type: "String"; value: string }
    | { type: "Number"; value: number };

}

export type _IAPIProcessObject =
    | { [key: string]: IAPIProcessObject }
    | { [key: string]: { type: "Boolean"; value: boolean } }
    | { [key: string]: { type: "String"; value: string } }
    | { [key: string]: { type: "Number"; value: number } };


export type TAddBasicInfoDataCompleteVariable = {
    responsible: { type: "Boolean"; value: boolean };
};

export type TCreateNegativeResponseCompleteVariable = {
    response_created: { type: "Boolean"; value: boolean };
};

export type TCheckAndFormulateResponseCompleteVariable = {
    response_created: { type: "Boolean"; value: boolean };
    data_complete: { type: "Boolean"; value: boolean };
};

export type TApproveStatementCompleteVariable = {
    approved_statement: { type: "Boolean"; value: boolean };
};

export type TCompleteTaskVariable = NonNullable<unknown>
| TAddBasicInfoDataCompleteVariable
| TCreateNegativeResponseCompleteVariable
| TCheckAndFormulateResponseCompleteVariable
| TApproveStatementCompleteVariable;
