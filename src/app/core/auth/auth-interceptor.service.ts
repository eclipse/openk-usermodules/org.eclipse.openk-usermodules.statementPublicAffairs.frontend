/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {tap} from "rxjs/operators";
import {EHttpStatusCodes, isHttpErrorWithStatus, urlJoin} from "../../util";
import {SPA_BACKEND_ROUTE} from "../external-routes";
import {AuthService} from "./auth.service";

/**
 * This service adds the access token to the headers for all backend requests if needed.
 */
@Injectable({providedIn: "root"})
export class AuthInterceptorService implements HttpInterceptor {

    public static HTTP_INTERCEPTOR_PROVIDER = {
        provide: HTTP_INTERCEPTORS,
        useExisting: AuthInterceptorService,
        multi: true
    };

    public unauthorized$: Observable<HttpErrorResponse>;

    private readonly unauthorizedSubject = new Subject<HttpErrorResponse>();

    private readonly unauthorizedEndpoints = [
        "version"
    ];

    public constructor(
        private readonly authService: AuthService,
        @Inject(SPA_BACKEND_ROUTE) private readonly baseUrl: string
    ) {
        this.unauthorized$ = this.unauthorizedSubject.asObservable();
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const needsAuthorization = this.needsAuthorization(req.url);
        return next
            .handle(needsAuthorization ? this.addToken(req) : req)
            .pipe(tap({error: (httpEvent) => needsAuthorization ? this.onUnauthorized(httpEvent) : null}));
    }

    public needsAuthorization(url: string): boolean {
        if (!url.startsWith(this.baseUrl)) {
            return false;
        }

        for (const endpoint of this.unauthorizedEndpoints) {
            if (url === urlJoin(this.baseUrl, endpoint)) {
                return false;
            }
        }

        return true;
    }

    public addToken(req: HttpRequest<any>): HttpRequest<any> {
        const token = this.authService.token;

        return token == null ? req : req.clone({
            headers: req.headers.append("Authorization", "Bearer " + token)
        });
    }

    public onUnauthorized(httpErrorResponse: HttpErrorResponse) {
        if (isHttpErrorWithStatus(httpErrorResponse, EHttpStatusCodes.UNAUTHORIZED)) {
            this.authService.clear();
            this.unauthorizedSubject.next(httpErrorResponse);
        }
    }

}
