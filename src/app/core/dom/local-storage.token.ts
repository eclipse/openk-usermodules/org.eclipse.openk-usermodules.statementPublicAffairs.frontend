/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";

/**
 * Injection token for the local storage object
 */
export const LOCAL_STORAGE = new InjectionToken<Storage>("Local storage object", {
    providedIn: "root",
    factory: () => localStorage
});
