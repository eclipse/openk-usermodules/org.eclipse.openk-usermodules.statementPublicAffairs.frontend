/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {action} from "@storybook/addon-actions";
import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core/i18n";
import {createSelectOptionsMock, createStatementModelMock} from "../../../../test";
import {StatementTableModule} from "../statement-table.module";

storiesOf("Shared / Layout", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            RouterTestingModule,
            I18nModule,
            StatementTableModule
        ]
    }))
    .add("StatementTableComponent", () => ({
        template: `
            <div style="padding: 1em; height: 100%; box-sizing: border-box;">
                <app-statement-table class="openk-table---without-last-border" style="width: 100%; height: 100%;"
                    [appDisabled]="appDisabled"
                    [appEntries]="appEntries"
                    [appStatementTypeOptions]="appStatementTypeOptions"
                    [style.maxHeight]="maxHeight"
                    (appSelect)="appToggleSelect($event)">
                </app-statement-table>
            </div>
        `,
        props: {
            restricted: boolean("Restrict columns", false),
            maxHeight: text("maxHeight", "initial"),
            appDisabled: boolean("appDisabled", false),
            appStatementTypeOptions: createSelectOptionsMock(5, "StatementType"),
            appToggleSelect: action("appToggleSelect"),
            appEntries: Array(19).fill(0)
                .map((_, id) => createStatementModelMock(id, id % 5)),
            columnsAll: ["select", "id", "title", "type", "date", "city", "district", "link"],
            columnsRestricted: ["id", "title", "type", "date", "city", "district", "link"],
        }
    }));
