/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {I18nModule, IAPITextBlockModel} from "../../../../core";
import {TextBlockModule} from "../../text-block.module";
import {TextBlocksListComponent} from "./text-block-list.component";

describe("TextBlocksListComponent", () => {
    let component: TextBlocksListComponent;
    let fixture: ComponentFixture<TextBlocksListComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                TextBlockModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlocksListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should add and remove a duplicate element to the list if appDuplicateOnDrag is set when starting to drag", () => {

        const element: IAPITextBlockModel = {
            id: "2",
            text: "different text",
            excludes: [],
            requires: []
        };

        const listData: IAPITextBlockModel[] = [
            {id: "1", text: "some text", excludes: [], requires: []},
            element,
            {id: "3", text: "more text", excludes: [], requires: []}
        ];

        component.appDuplicateOnDrag = true;
        component.appListData = listData;
        component.startDrag(element);
        expect(component.appListData).toEqual([
            {id: "1", text: "some text", excludes: [], requires: []},
            element,
            element,
            {id: "3", text: "more text", excludes: [], requires: []}
        ]);
        component.stopDrag(element);
        expect(component.appListData).toEqual(listData);

        component.appDuplicateOnDrag = false;
        component.startDrag(element);
        expect(component.appListData).toEqual(listData);
    });
});
