/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextArrangementItemModel, IAPITextBlockModel} from "../../../../core/api/text";
import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";
import {GetBlockDataFromArrangementPipe} from "./get-blockdata-array-from-arrangement.pipe";


describe("GetBlockDataFromArrangementPipe", () => {

    const pipe = new GetBlockDataFromArrangementPipe();

    it("should return empty array for missing arrangement and blockmodel values", () => {
        let result = pipe.transform(
            undefined, undefined, undefined, undefined, undefined);
        expect(result).toEqual([]);
        result = pipe.transform(
            null, null, null, null, null);
        expect(result).toEqual([]);
    });

    it("should return one block with type text and the replacement as value when set", () => {

        const arrangement: IAPITextArrangementItemModel = {
            type: "block",
            placeholderValues: {},
            textblockId: "textblockId"
        };

        const blockModel: IAPITextBlockModel = {
            id: "textblockId",
            text: "text",
            excludes: [],
            requires: []
        };

        let result = pipe.transform(
            arrangement, blockModel, undefined, undefined, undefined);
        expect(JSON.parse(JSON.stringify(result))).toEqual([{type: "text", value: "text"}]);

        result = pipe.transform(
            {...arrangement, replacement: "replacement-text"}, blockModel, undefined, undefined, undefined);
        expect(JSON.parse(JSON.stringify(result))).toEqual([{type: "text", value: "replacement-text"}]);
    });

    it("should return empty array for newline and pagebreak blocks so no text is displayed for those", () => {
        const arrangement: IAPITextArrangementItemModel = {
            type: "block",
            placeholderValues: {}
        };

        let result = pipe.transform(
            {...arrangement, type: "newline"}, undefined, undefined, undefined, undefined);
        expect(JSON.parse(JSON.stringify(result))).toEqual([]);

        result = pipe.transform(
            {...arrangement, type: "pagebreak"}, undefined, undefined, undefined, undefined);
        expect(JSON.parse(JSON.stringify(result))).toEqual([]);

        result = pipe.transform(
            {...arrangement, type: "text"}, undefined, undefined, undefined, undefined);
        expect(JSON.parse(JSON.stringify(result))).toEqual([]);
    });

    describe("should replace textmarkers <>", () => {
        const arrangement: IAPITextArrangementItemModel = {
            type: "block",
            placeholderValues: {},
            textblockId: "textblockId"
        };

        const blockModel: IAPITextBlockModel = {
            id: "textblockId",
            text: "this is a test <f:freetext><t:replacementtext><s:select><d:date> and more text afterwards",
            excludes: [],
            requires: []
        };

        it("should split up textblockmodel text content into the different types and return them as array", () => {

            const expectedResult: ITextBlockRenderItem[] = [
                {
                    type: "text",
                    value: "this is a test "
                },
                {
                    type: "input",
                    value: "freetext"
                },
                {
                    type: "text-fill",
                    value: "replacementtext"
                },
                {
                    type: "select",
                    value: "select"
                },
                {
                    type: "date",
                    value: "date"
                },
                {
                    type: "text",
                    value: " and more text afterwards"
                }
            ];

            const result = pipe.transform(
                arrangement, blockModel, undefined, undefined, undefined);
            expect(JSON.parse(JSON.stringify(result))).toEqual(expectedResult);
        });

        it("should set the values correctly for the different types of display blocks", () => {

            const placeholderValues: { [key: string]: string } = {
                "<f:freetext>": "freeTextValue",
                "<s:select>": "Option 1",
                "<d:date>": "07-08-2020"
            };

            const replacementTexts: { [key: string]: string } = {
                replacementtext: "replaced"
            };

            const selects: { [key: string]: string[] } = {
                select: ["Option 1", "Option 2"]
            };

            const expectedResult: ITextBlockRenderItem[] = [
                {
                    type: "text",
                    value: "this is a test "
                },
                {
                    type: "input",
                    value: "freetext",
                    placeholder: "freeTextValue"
                },
                {
                    type: "text-fill",
                    value: "replacementtext",
                    placeholder: "replaced"
                },
                {
                    type: "select",
                    value: "select",
                    placeholder: "Option 1",
                    options: ["Option 1", "Option 2"]
                },
                {
                    type: "date",
                    value: "date",
                    placeholder: "07-08-2020"
                },
                {
                    type: "text",
                    value: " and more text afterwards"
                }
            ];

            const result = pipe.transform(
                arrangement, blockModel, placeholderValues, replacementTexts, selects);
            expect(JSON.parse(JSON.stringify(result))).toEqual(expectedResult);
        });
    });

});

