/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";
import {CombineBlockdataTextPipe} from "./combine-blockdata-text.pipe";

describe("CombineBlockdataTextkPipe", () => {

    const pipe = new CombineBlockdataTextPipe();

    it("should return empty array for missing input (undefined/null)", () => {
        let result = pipe.transform(undefined);
        expect(result).toEqual([]);

        result = pipe.transform(null);
        expect(result).toEqual([]);
    });

    it("should return a new array combining the entries of type text", () => {
        const input: ITextBlockRenderItem[] = [
            {type: "newline", value: ""},
            {type: "text", value: "first-text"},
            {type: "text", value: "second-text"},
            {type: "pagebreak", value: ""},
            {type: "text", value: "third-text"},
            {type: "select", value: "second-text", options: ["Option 1", "Option 2"], placeholder: "placeholder"},
            {type: "text", value: "fourth-text"}
        ];
        const expectedResult: ITextBlockRenderItem[] = [
            {type: "newline", value: ""},
            {type: "text", value: "first-text second-text"},
            {type: "pagebreak", value: ""},
            {type: "text", value: "third-text"},
            {type: "select", value: "second-text", options: ["Option 1", "Option 2"], placeholder: "placeholder"},
            {type: "text", value: "fourth-text"}
        ];
        const result = pipe.transform(input);
        expect(result).toEqual(expectedResult);
    });
});
