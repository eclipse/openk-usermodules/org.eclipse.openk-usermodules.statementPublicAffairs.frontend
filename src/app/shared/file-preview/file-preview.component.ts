/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Inject, Input, OnChanges, OnDestroy, SimpleChanges} from "@angular/core";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {URL_TOKEN} from "../../core/dom";

@Component({
    selector: "app-file-preview",
    templateUrl: "./file-preview.component.html",
    styleUrls: ["./file-preview.component.scss"]
})
export class FilePreviewComponent implements OnChanges, OnDestroy {

    @Input()
    public appFile: File;

    public pdfUrl: SafeResourceUrl;

    public objectUrl: string;

    public constructor(
        public sanitizer: DomSanitizer,
        @Inject(URL_TOKEN) public url: typeof URL
    ) {

    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes.appFile) {
            if (this.objectUrl) {
                this.url.revokeObjectURL(this.objectUrl);
                this.objectUrl = null;
                this.pdfUrl = null;
            }
            this.pdfUrl = this.getSafeUrl();
        }
    }

    public ngOnDestroy() {
        if (this.objectUrl) {
            this.url.revokeObjectURL(this.objectUrl);
        }
    }

    public getSafeUrl() {
        this.objectUrl = this.appFile ? this.url.createObjectURL(this.appFile) : null;
        return this.objectUrl ? this.sanitizer.bypassSecurityTrustResourceUrl(this.objectUrl + "#view=Fit") : null;
    }

}
