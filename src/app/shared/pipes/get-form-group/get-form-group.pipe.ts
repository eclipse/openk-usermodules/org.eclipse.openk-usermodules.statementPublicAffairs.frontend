/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {AbstractControl, FormGroup} from "@angular/forms";

@Pipe({
    name: "getFormGroup"
})
export class GetFormGroupPipe implements PipeTransform {

    public transform(group: FormGroup | AbstractControl, key: Array<string | number> | string, getControls?: boolean): FormGroup {
        const control = group?.get(key);
        return control instanceof FormGroup ? control : undefined;
    }

}
