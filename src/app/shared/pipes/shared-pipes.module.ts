/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {FindElementInArrayPipe} from "./find-element-in-array";
import {GetFormArrayPipe} from "./get-form-array";
import {GetFormErrorPipe} from "./get-form-error";
import {GetFormGroupPipe} from "./get-form-group";
import {ObjKeysToArrayPipe} from "./obj-keys-to-array";
import {ObjToArrayPipe} from "./obj-to-array";
import {PairPipe} from "./pair";
import {StringsToOptionsPipe} from "./strings-to-options";

@NgModule({
    declarations: [
        ObjToArrayPipe,
        ObjKeysToArrayPipe,
        PairPipe,
        GetFormArrayPipe,
        GetFormGroupPipe,
        GetFormErrorPipe,
        FindElementInArrayPipe,
        StringsToOptionsPipe
    ],
    exports: [
        ObjToArrayPipe,
        ObjKeysToArrayPipe,
        PairPipe,
        GetFormArrayPipe,
        GetFormGroupPipe,
        GetFormErrorPipe,
        FindElementInArrayPipe,
        StringsToOptionsPipe
    ]
})
export class SharedPipesModule {

}
