/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TranslateModule} from "@ngx-translate/core";
import {environment} from "../environments/environment";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {CoreModule} from "./core";
import {AppNavigationFrameModule} from "./features/navigation";
import {AppStoreModule} from "./store";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        TranslateModule,

        AppRoutingModule,
        CoreModule,
        AppStoreModule,
        AppNavigationFrameModule,

        // This import is only important for development; in production, nothing is imported.
        // ! This import must come after AppStoreModule in order make the NGRX Store Devtools available. !
        ...environment.imports
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {

}
