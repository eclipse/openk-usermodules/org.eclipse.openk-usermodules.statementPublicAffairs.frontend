/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIStatementModel} from "../core";

export function createStatementModelMock(id: number, typeId: number = 1): IAPIStatementModel {
    return {
        id,
        title: "Title " + id,
        departmentsDueDate: "2019-09-11",
        dueDate: "2019-09-11",
        receiptDate: "2019-09-10",
        creationDate: "2019-09-09",
        finished: true,
        typeId,
        city: "Darmstadt",
        district: "Heppenheim",
        contactId: "ABCD",
        sourceMailId: null
    };
}
