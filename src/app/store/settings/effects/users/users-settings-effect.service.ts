/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {endWith, map, startWith, switchMap} from "rxjs/operators";
import {IAPIUserSettings, SettingsApiService} from "../../../../core";
import {catchHttpErrorTo, EHttpStatusCodes} from "../../../../util";
import {catchErrorTo} from "../../../../util/rxjs";
import {EErrorCode, setErrorAction} from "../../../root";
import {
    fetchUsersAction,
    setSettingsLoadingStateAction,
    setUsersDataAction,
    submitUserSettingsAction,
    syncUserDataAction
} from "../../actions";

@Injectable({providedIn: "root"})
export class UsersSettingsEffect {

    public submit$ = createEffect(() => this.actions.pipe(
        ofType(submitUserSettingsAction),
        switchMap((action) => this.submit(action.userId, {...action.data}))
    ));

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchUsersAction),
        switchMap(() => this.fetch())
    ));

    public sync$ = createEffect(() => this.actions.pipe(
        ofType(syncUserDataAction),
        switchMap(() => this.sync())
    ));

    public constructor(public actions: Actions, public settingsApiService: SettingsApiService) {
    }

    public sync(): Observable<Action> {
        return this.settingsApiService.syncUserData().pipe(
            switchMap(() => this.fetch()),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {fetchingUsers: true}})),
            endWith(setSettingsLoadingStateAction({state: {fetchingUsers: false}}))
        );
    }

    public fetch(): Observable<Action> {
        return this.settingsApiService.fetchUsers().pipe(
            map((data) => setUsersDataAction({data})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {fetchingUsers: true}})),
            endWith(setSettingsLoadingStateAction({state: {fetchingUsers: false}}))
        );
    }

    public submit(userId: number, data: IAPIUserSettings): Observable<Action> {
        const isDepartmentSet = data.departments != null && data.departments.length > 0;

        const departments = isDepartmentSet ? data.departments : undefined;
        data = {
            ...data,
            departments
        };
        return this.settingsApiService.setUserData(userId, data).pipe(
            switchMap(() => this.fetch()),
            catchHttpErrorTo(setErrorAction({error: EErrorCode.BAD_USER_DATA}), EHttpStatusCodes.BAD_REQUEST),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {submittingUserData: true}})),
            endWith(setSettingsLoadingStateAction({state: {submittingUserData: false}}))
        );
    }

}
