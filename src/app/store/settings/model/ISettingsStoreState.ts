/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {
    IAPIDepartmentTable,
    IAPISectorsModel,
    IAPIStatementType,
    IAPITextBlockConfigurationModel,
    IAPIUserInfoExtended
} from "../../../core";
import {TStoreEntities} from "../../../util";
import {ISettingsLoadingState} from "./ISettingsLoadingState";

export interface ISettingsStoreState {

    /**
     * Object with all statement types; the key is the unique ID of the type in the back end data base.
     */
    statementTypes: TStoreEntities<IAPIStatementType>;

    /**
     * Object with all the available sectors for newly created statements.
     */
    sectors: IAPISectorsModel;

    /**
     * Object with all departments and sectors; the key is a pair of city and district concatenated with a #.
     */
    departments: IAPIDepartmentTable;

    /**
     * Loading object which indicates which part of the application is loading.
     */
    loading?: ISettingsLoadingState;

    /**
     * Textblock configuration object for newly created statements.
     */
    textblock: IAPITextBlockConfigurationModel;

    /**
     * List of all users with access to the app
     */
    users: IAPIUserInfoExtended[];

}
