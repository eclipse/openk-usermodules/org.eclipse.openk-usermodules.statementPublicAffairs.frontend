/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPISectorsModel, IAPIStatementType} from "../../../core";
import {ISettingsLoadingState} from "../model";

export const fetchSettingsAction = createAction(
    "[New] Fetch settings"
);

export const setStatementTypesAction = createAction(
    "[API] Set statement types",
    props<{ statementTypes: IAPIStatementType[] }>()
);

export const setSectorsAction = createAction(
    "[API] Get sectors",
    props<{ sectors: IAPISectorsModel }>()
);

export const setSettingsLoadingStateAction = createAction(
    "[App/API] Set settings loading state",
    props<{ state: ISettingsLoadingState }>()
);
