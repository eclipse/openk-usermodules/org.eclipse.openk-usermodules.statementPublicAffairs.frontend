/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPITextBlockConfigurationModel} from "../../../core/api/text";

export const fetchTextblockSettingsAction = createAction(
    "[Settings] Fetch text block settings"
);

export const setTextblockSettingsAction = createAction(
    "[API] Set text block settings",
    props<{ data: IAPITextBlockConfigurationModel }>()
);

export const submitTextblockSettingsAction = createAction(
    "[Settings] Submit text block settings",
    props<{ data: IAPITextBlockConfigurationModel }>()
);
