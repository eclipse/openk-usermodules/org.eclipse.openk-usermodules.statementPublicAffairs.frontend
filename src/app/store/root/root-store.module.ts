/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {APP_INITIALIZER, NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {Store, StoreModule} from "@ngrx/store";
import {intializeAction} from "./actions";
import {InitializationEffect, KeepAliveEffect, OpenNewTabEffect, RouterEffects, ToastEffect, UserEffect, VersionEffect} from "./effects";
import {ROOT_REDUCER} from "./root-reducers.token";

export function dispatchInitialization(store: Store) {
    return async () => store.dispatch(intializeAction());
}

@NgModule({
    imports: [
        StoreModule.forRoot(ROOT_REDUCER),
        EffectsModule.forRoot([
            InitializationEffect,
            KeepAliveEffect,
            OpenNewTabEffect,
            RouterEffects,
            UserEffect,
            VersionEffect,
            ToastEffect
        ])
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: dispatchInitialization,
            deps: [Store],
            multi: true
        }
    ]
})
export class RootStoreModule {

}
