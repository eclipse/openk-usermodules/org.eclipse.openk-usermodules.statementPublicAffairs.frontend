/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {IRootStoreState} from "../model";

export const rootStateSelector = (state: IRootStoreState) => state != null ? state : {};

export const isLoadingSelector = createSelector(
    rootStateSelector,
    (state) => state.isLoading
);

export const exitCodeSelector = createSelector(
    rootStateSelector,
    (state) => state.exitCode
);

export const errorCodeSelector = createSelector(
    rootStateSelector,
    (state) => state.error
);
