/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export enum EErrorCode {
    UNEXPECTED = "shared.errorMessages.unexpected",
    TASK_TO_COMPLETE_NOT_FOUND = "shared.errorMessages.taskToCompleteNotFound",
    ALREADY_CLAIMED = "shared.errorMessages.alreadyClaimed",
    CLAIMED_BY_OTHER_USER = "shared.errorMessages.claimedByAnotherUser",
    MISSING_FORM_DATA = "shared.errorMessages.missingFormData",
    FAILED_LOADING_CONTACT = "shared.errorMessages.failedLoadingContact",
    CONTACT_MODULE_NO_ACCESS = "shared.errorMessages.noAccessToContactModule",
    FAILED_FILE_UPLOAD = "shared.errorMessages.failedFileUpload",
    FAILED_MAIL_TRANSFER = "shared.errorMessages.failedMailTransfer",
    INVALID_TEXT_ARRANGEMENT = "shared.errorMessages.invalidTextArrangement",
    COULD_NOT_LOAD_MAIL_DATA = "shared.errorMessages.couldNotLoadMailData",
    COULD_NOT_SEND_MAIL = "shared.errorMessages.couldNotSendMail",
    COULD_NOT_EDIT_TAG = "shared.errorMessages.couldNotEditTag",
    COULD_NOT_EDIT_TYPE = "shared.errorMessages.couldNotEditType",
    INVALID_FILE_FORMAT = "shared.errorMessages.invalidFileFormat",
    SEARCH_NO_RESULT = "shared.errorMessages.searchNoResult",
    BAD_USER_DATA = "shared.errorMessages.badUserData",
    INVALID_DEPARTMENTS_DUE_DATE = "shared.errorMessages.invalidDepartmentsDueDate",
    TASK_NOT_CLAIMED_BY_USER = "shared.errorMessages.taskNotClaimedByUser",
    DEPARTMENT_MULTIPLE_USE = "shared.errorMessages.departmentMultipleUse"
}
