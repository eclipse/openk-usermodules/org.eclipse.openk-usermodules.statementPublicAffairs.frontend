/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {toggleLoadingPageAction} from "../actions";
import {isLoadingReducer} from "./is-loading.reducer";

describe("isLoadingReducer", () => {

    it("should set the isLoading state to the given value, false for no value", () => {
        let initialState = true;
        let action = toggleLoadingPageAction({isLoading: false});
        let state = isLoadingReducer(initialState, action);
        expect(state).toBeFalse();

        initialState = true;
        action = toggleLoadingPageAction({});
        state = isLoadingReducer(initialState, action);
        expect(state).toBeFalse();

        initialState = true;
        action = toggleLoadingPageAction(null);
        state = isLoadingReducer(initialState, action);
        expect(state).toBeFalse();

        initialState = true;
        action = toggleLoadingPageAction({isLoading: null});
        state = isLoadingReducer(initialState, action);
        expect(state).toBeFalse();

        initialState = false;
        action = toggleLoadingPageAction({isLoading: true});
        state = isLoadingReducer(initialState, action);
        expect(state).toBeTrue();
    });

});
