/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createFeatureSelector, createSelector} from "@ngrx/store";
import {
    arrayJoin,
    filterDistinctValues,
    selectArrayProjector,
    selectEntityWithIdProjector,
    selectPropertyProjector
} from "../../../util/store";
import {queryParamsMailIdSelector} from "../../root/selectors";
import {statementMailIdSelector} from "../../statements/selectors";
import {EMAIL_NAME} from "../mail-reducers.token";
import {IMailStoreState} from "../model";

export const emailStateSelector = createFeatureSelector<IMailStoreState>(EMAIL_NAME);

const emailEntitiesSelector = createSelector(
    emailStateSelector,
    selectPropertyProjector("entities", {})
);

const emailInboxIdsSelector = createSelector(
    emailStateSelector,
    selectArrayProjector("inboxIds", [])
);

export const getIsEmailInInboxSelector = createSelector(
    emailInboxIdsSelector,
    (inbox) => arrayJoin(inbox).length > 0
);

export const getEmailInboxSelector = createSelector(
    emailEntitiesSelector,
    emailInboxIdsSelector,
    (entities, inboxIds) => filterDistinctValues(arrayJoin(inboxIds).map((mailId) => entities[mailId])).reverse()
);

export const getSelectedEmailSelector = createSelector(
    emailEntitiesSelector,
    queryParamsMailIdSelector,
    selectEntityWithIdProjector()
);

export const getStatementMailSelector = createSelector(
    emailEntitiesSelector,
    statementMailIdSelector,
    selectEntityWithIdProjector()
);

export const getEmailLoadingSelector = createSelector(
    emailStateSelector,
    selectPropertyProjector("loading")
);
