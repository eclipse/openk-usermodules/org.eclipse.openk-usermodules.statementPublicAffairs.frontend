/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {setEmailLoadingStateAction} from "../../actions";
import {IMailLoadingState} from "../../model";
import {mailLoadingReducer} from "./mail-loading.reducer";

describe("mailLoadingReducer", () => {

    let initialState: IMailLoadingState;
    let state: IMailLoadingState;
    let action: Action;

    it("should update loading state on setEmailInboxAction", () => {
        initialState = undefined;
        action = setEmailLoadingStateAction(null);
        state = mailLoadingReducer(initialState, action);
        expect(state).toEqual(undefined);

        action = setEmailLoadingStateAction({loading: null});
        state = mailLoadingReducer(initialState, action);
        expect(state).toEqual(undefined);

        action = setEmailLoadingStateAction({loading: {fetching: false}});
        state = mailLoadingReducer(initialState, action);
        expect(state).toEqual(undefined);

        action = setEmailLoadingStateAction({loading: {fetching: true}});
        state = mailLoadingReducer(initialState, action);
        expect(state).toEqual({fetching: true});
    });

});
