/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {merge, Observable} from "rxjs";
import {filter, map, retry, switchMap} from "rxjs/operators";
import {TextApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {fetchStatementTextArrangementAction, updateStatementConfigurationAction, updateStatementEntityAction} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchTextArrangementEffect {

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchStatementTextArrangementAction),
        filter((action) => action.statementId != null),
        switchMap((action) => this.fetch(action.statementId))
    ));

    public constructor(private readonly actions: Actions, private readonly textApiService: TextApiService) {

    }

    public fetch(statementId: number): Observable<Action> {
        return merge(
            this.fetchArrangement(statementId),
            this.fetchConfiguration(statementId)
        );
    }

    public fetchArrangement(statementId: number) {
        return this.textApiService.getArrangement(statementId).pipe(
            map((arrangement) => updateStatementEntityAction({statementId, entity: {arrangement}})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

    public fetchConfiguration(statementId: number) {
        return this.textApiService.getConfiguration(statementId).pipe(
            map((text) => updateStatementConfigurationAction({statementId, entity: {text}})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

}

