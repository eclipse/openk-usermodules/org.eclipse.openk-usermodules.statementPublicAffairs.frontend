/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {setStatementLoadingAction} from "../../actions";
import {IStatementLoadingEntity} from "../../model";
import {statementLoadingReducer} from "./statement-loading.reducer";

describe("statementLoadingReducer", () => {

    it("should update loading state  ", () => {
        const initialState: IStatementLoadingEntity = {search: true};
        let action = setStatementLoadingAction({loading: {}});
        let state = statementLoadingReducer(initialState, action);
        expect(state).toEqual(initialState);
        action = setStatementLoadingAction({loading: {search: false}});
        state = statementLoadingReducer(initialState, action);
        expect(state).toEqual({...initialState, search: false});
    });

});
