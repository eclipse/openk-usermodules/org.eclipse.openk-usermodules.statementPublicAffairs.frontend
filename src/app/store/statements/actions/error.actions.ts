/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPITextArrangementErrorModel} from "../../../core/api/text";
import {IStatementErrorEntity} from "../model";

export const setStatementErrorAction = createAction(
    "[API] Set statement error",
    props<{ statementId: number | "new"; error: IStatementErrorEntity }>()
);

export const setStatementArrangementErrorAction = createAction(
    "[API] Set statement arrangement error",
    props<{ statementId: number; error: IAPITextArrangementErrorModel[] }>()
);
