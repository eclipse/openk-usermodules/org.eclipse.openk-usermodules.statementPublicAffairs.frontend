/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {statementEntitiesSelector} from "../statement.selectors";
import {getStatementSearchSelector} from "../statements-store-state.selectors";

export const getSearchContentStatementsSelector = createSelector(
    statementEntitiesSelector,
    getStatementSearchSelector,
    (entities, search) => {
        if (Array.isArray(search?.content) && entities != null) {
            return search.content
                .map((id) => entities[id]?.info)
                .filter((_) => _ != null);
        }
        return [];
    }
);

export const getSearchContentInfoSelector = createSelector(
    getStatementSearchSelector,
    (search) => ({
        totalPages: search?.totalPages,
        currentPage: search?.number,
        size: search?.size
    })
);
