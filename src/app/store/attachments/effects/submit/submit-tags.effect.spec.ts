/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, of, Subscription} from "rxjs";
import {SPA_BACKEND_ROUTE} from "../../../../core";
import {submitTagsAction} from "../../../statements";
import {SubmitTagsEffect} from "./submit-tags.effect";

describe("SubmitTagsEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: SubmitTagsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                SubmitTagsEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(SubmitTagsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should call submit method on submit action", async () => {
        const submitSpy = spyOn(effect, "submit").and.returnValue(EMPTY);
        const tags = [{label: "label"}];
        actions$ = of(submitTagsAction({tags}));
        subscription = effect.submit$.subscribe();
        expect(submitSpy).toHaveBeenCalledWith(tags);
    });

    it("should call delete tags method on submit action", async () => {
        const deleteSpy = spyOn(effect, "deleteTags").and.returnValue(EMPTY);
        const tags = [{label: "label", delete: true}];
        actions$ = of(submitTagsAction({tags}));
        subscription = effect.submit$.subscribe();
        expect(deleteSpy).toHaveBeenCalledWith(["label"], []);
    });

    it("should call add tags method on submit action", async () => {
        const addSpy = spyOn(effect, "addTags").and.returnValue(EMPTY);
        const tags = [{label: "label", add: true}];
        actions$ = of(submitTagsAction({tags}));
        subscription = effect.submit$.subscribe();
        expect(addSpy).toHaveBeenCalledWith(["label"], []);
    });
});

