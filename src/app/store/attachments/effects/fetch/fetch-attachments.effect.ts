/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {merge, Observable} from "rxjs";
import {filter, map, retry, switchMap} from "rxjs/operators";
import {AttachmentsApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {completeInitializationAction, setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {fetchAttachmentsAction, fetchAttachmentTagsAction, setAttachmentsAction, setAttachmentTagsAction} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchAttachmentsEffect {

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchAttachmentsAction),
        filter((action) => action.statementId != null),
        switchMap((action) => this.fetch(action.statementId))
    ));

    public fetchTags$ = createEffect(() => this.actions.pipe(
        ofType(fetchAttachmentTagsAction, completeInitializationAction),
        switchMap(() => this.fetchAttachmentTags())
    ));

    public constructor(public actions: Actions, private attachmentsApiService: AttachmentsApiService) {

    }

    public fetch(statementId: number) {
        return merge(
            this.fetchAttachments(statementId),
            this.fetchAttachmentTags()
        );
    }

    public fetchAttachments(statementId: number): Observable<Action> {
        return this.attachmentsApiService.getAttachments(statementId).pipe(
            map((entities) => setAttachmentsAction({statementId, entities})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

    public fetchAttachmentTags(): Observable<Action> {
        return this.attachmentsApiService.getTagList().pipe(
            map((tags) => setAttachmentTagsAction({tags})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

}
