/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {IAPIAttachmentModel} from "../../../../core/api/attachments";
import {createAttachmentModelMock} from "../../../../test";
import {addAttachmentEntityAction, deleteAttachmentsAction, setAttachmentsAction, updateAttachmentTagsAction} from "../../actions";
import {IAttachmentsStoreState} from "../../model";
import {attachmentEntitiesReducer} from "./attachment-entities.reducer";

describe("attachmentEntitiesReducer", () => {

    const initialState: IAttachmentsStoreState["entities"] = {
        17: createAttachmentModelMock(17, "tag0"),
        18: createAttachmentModelMock(18)
    };

    it("should set a list of attachment entities to store", () => {
        const attachment = createAttachmentModelMock(19, "1", "9");
        const action: Action = setAttachmentsAction({
            statementId: 1919,
            entities: [attachment]
        });
        const state = attachmentEntitiesReducer(initialState, action);
        expect(state).toEqual({
            ...initialState,
            19: attachment
        });
    });

    it("should add a single attachment entitiy to store", () => {
        const attachment = createAttachmentModelMock(19, "1", "9");
        const action: Action = addAttachmentEntityAction({
            statementId: 1919,
            entity: attachment
        });
        const state = attachmentEntitiesReducer(initialState, action);
        expect(state).toEqual({
            ...initialState,
            19: attachment
        });
    });

    it("should delete attachment entities from store", () => {
        const action: Action = deleteAttachmentsAction({
            statementId: 1919,
            entityIds: [17]
        });
        const state = attachmentEntitiesReducer(initialState, action);
        expect(state).toEqual({
            18: initialState[18]
        });
    });

    it("should update the tag list of attachments", () => {
        const action = updateAttachmentTagsAction({
            items: [
                {
                    id: 17,
                    tagIds: ["tag19"]
                },
                {
                    id: 19,
                    tagIds: ["tag19"]
                }
            ]
        });
        const state = attachmentEntitiesReducer(initialState, action);
        expect(state).toEqual({
            ...initialState,
            17: {
                ...initialState[17],
                tagIds: ["tag19"]
            },
            19: {
                id: 19,
                tagIds: ["tag19"]
            } as IAPIAttachmentModel
        });
    });

});
