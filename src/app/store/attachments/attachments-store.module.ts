/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {ATTACHMENTS_NAME, ATTACHMENTS_REDUCER} from "./attachments-reducers.token";
import {FetchAttachmentsEffect, SubmitAttachmentsEffect} from "./effects";
import {AttachmentDownloadEffect} from "./effects/download";
import {SubmitConsiderationsEffect} from "./effects/submit/submit-considerations.effect";
import {SubmitTagsEffect} from "./effects/submit/submit-tags.effect";

@NgModule({
    imports: [
        StoreModule.forFeature(ATTACHMENTS_NAME, ATTACHMENTS_REDUCER),
        EffectsModule.forFeature([
            AttachmentDownloadEffect,
            FetchAttachmentsEffect,
            SubmitAttachmentsEffect,
            SubmitConsiderationsEffect,
            SubmitTagsEffect
        ])
    ]
})
export class AttachmentsStoreModule {

}
