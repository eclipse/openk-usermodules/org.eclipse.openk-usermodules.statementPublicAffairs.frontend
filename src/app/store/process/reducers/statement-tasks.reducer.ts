/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {arrayJoin, filterDistinctValues, TStoreEntities} from "../../../util/store";
import {deleteTaskAction, setStatementTasksAction, setTaskEntityAction} from "../actions";

export const statementTaskReducer = createReducer<TStoreEntities<string[]>>(
    {},
    on(setStatementTasksAction, (state, payload) => {
        const statementId = payload?.statementId;

        if (typeof statementId !== "number") {
            return state;
        }

        const taskIds = arrayJoin(payload.tasks)
            .filter((task) => task?.statementId === statementId && typeof task?.taskId === "string")
            .map((task) => task.taskId);

        return {
            ...state,
            [statementId]: taskIds.length === 0 ? undefined : filterDistinctValues(taskIds)
        };
    }),
    on(setTaskEntityAction, (state, payload) => {
        const statementId = payload.task?.statementId;
        const taskId = payload.task?.taskId;
        return statementId == null ? state : {
            ...state,
            [statementId]: filterDistinctValues(arrayJoin(state[statementId], [taskId]))
        };
    }),
    on(deleteTaskAction, (state, payload) => {
        if (typeof payload?.statementId !== "number") {
            return state;
        }

        const statementId = payload.statementId;
        const taskId = payload.taskId;
        const tasks = taskId == null ? [] : arrayJoin(state[statementId]).filter((t) => t !== taskId);

        return {
            ...state,
            [statementId]: tasks.length > 0 ? tasks : undefined
        };
    })
);
