/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {IAPIClaimDetails} from "../../../core/api/process/IAPIClaimDetails";
import {deleteEntities, TStoreEntities, updateEntitiesObject} from "../../../util";
import {deleteClaimDetailsAction, setClaimDetailsAction} from "../actions";

export const claimDetailsReducer = createReducer<TStoreEntities<IAPIClaimDetails>>(
    {},
    on(setClaimDetailsAction, (state, payload) => updateEntitiesObject(state, [payload.claimDetails], (claimDetails) => claimDetails.taskId)),
    on(deleteClaimDetailsAction, (state, payload) => {
        if (typeof payload?.taskId !== "string" || payload.taskId == null) {
            return state;
        }
        return deleteEntities(state, [payload.taskId]);
    })
);
