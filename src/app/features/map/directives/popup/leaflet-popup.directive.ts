/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, EmbeddedViewRef, Input, NgZone, OnChanges, OnDestroy, SimpleChanges, TemplateRef} from "@angular/core";
import {LatLngExpression, Popup} from "leaflet";
import {stringToLatLngZoom} from "../../util";
import {LeafletHandler} from "../leaflet";

/**
 * This structural directive renders an Angular template as a popup in a leaflet map instance.
 * Since leaflet is running outside Angular's zone, change detection must be either triggered manually via the updateView method or
 * by providing an input for appLeafletPopupData which is then tracked by Angular itself.
 *
 * The popup is only rendered if proper coordinates are provided via the input appLeafletPopup.
 */
@Directive({selector: "[appLeafletPopup]"})
export class LeafletPopupDirective implements OnChanges, OnDestroy {

    /**
     * Data object which is tracked by Angular and updates the view.
     */
    @Input()
    public appLeafletPopupData: any;

    public readonly embeddedViewRef: EmbeddedViewRef<any>;

    public readonly popup: Popup = new Popup({closeOnClick: false, offset: [0.25, -11]});

    public constructor(
        public readonly templateRef: TemplateRef<any>,
        public readonly ngZone: NgZone,
        public readonly leafletHandler: LeafletHandler
    ) {
        this.embeddedViewRef = this.templateRef.createEmbeddedView({});
        this.embeddedViewRef.detectChanges();
        const content = this.embeddedViewRef.rootNodes[0];
        this.ngZone.run(() => this.popup.setContent(content));
    }

    /**
     * If proper coordinates are provided, the leaflet popup instance is opened and set to the given coordinates.
     * Otherwise, the popup is removed from the map.
     */
    @Input()
    public set appLeafletPopup(latLngZoom: string) {
        const position = stringToLatLngZoom(latLngZoom);
        if (position == null) {
            this.close();
        } else {
            this.setLatLng(position);
            this.open();
        }
    }

    public ngOnDestroy() {
        this.close();
        this.embeddedViewRef.destroy();
    }

    public ngOnChanges(changes: SimpleChanges) {
        const updateOn: Array<keyof LeafletPopupDirective> = ["appLeafletPopupData"];
        if (updateOn.some((key) => changes[key] != null)) {
            this.updateView();
        }
    }

    /**
     * Triggers the change detection for the embedded view provided by the template ref and updates also the leaflet popup instance.
     */
    public updateView() {
        this.embeddedViewRef.detectChanges();
        this.ngZone.runOutsideAngular(() => this.popup.update());
    }

    public close() {
        this.ngZone.runOutsideAngular(() => this.popup.removeFrom(this.leafletHandler.instance));
    }

    public open() {
        this.ngZone.runOutsideAngular(() => this.popup.openOn(this.leafletHandler.instance));
    }

    public setLatLng(latLng: LatLngExpression) {
        this.ngZone.runOutsideAngular(() => this.popup.setLatLng(latLng));
    }

}
