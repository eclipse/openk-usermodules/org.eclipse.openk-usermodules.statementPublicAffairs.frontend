/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {DateControlModule} from "../../shared/controls/date-control";
import {SelectModule} from "../../shared/controls/select";
import {ActionButtonModule} from "../../shared/layout/action-button";
import {CardModule} from "../../shared/layout/card";
import {CollapsibleModule} from "../../shared/layout/collapsible";
import {SideMenuModule} from "../../shared/layout/side-menu";
import {StatementTableModule} from "../../shared/layout/statement-table";
import {SharedPipesModule} from "../../shared/pipes";
import {AttachmentsFormModule} from "../forms/attachments";
import {CommentsFormModule} from "../forms/comments";
import {StatementInformationFormModule} from "../forms/statement-information";
import {MapModule} from "../map";
import {
    ProcessDiagramComponent,
    ProcessHistoryComponent,
    ProcessInformationComponent,
    StatementDetailsAttachmentsComponent,
    StatementDetailsComponent,
    StatementDetailsContributionsComponent,
    StatementDetailsGeographicPositionComponent,
    StatementDetailsInformationComponent,
    StatementDetailsLinkedStatementsComponent,
    StatementDetailsSideMenuComponent
} from "./components";
import {StatementDetailsConsiderationsComponent} from "./components/considerations";
import {LinkedStatementsComponent} from "./components/linked-statements/linked-statements";
import {StatementDetailsOutboxComponent} from "./components/outbox/statement-details-outbox.component";
import {BpmnDirective} from "./directives";
import {GetProcessHistoryEntriesPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatIconModule,
        MatTableModule,
        TranslateModule,
        RouterModule,

        CardModule,
        SharedPipesModule,
        DateControlModule,
        CollapsibleModule,
        CommentsFormModule,
        SideMenuModule,
        ActionButtonModule,
        MapModule,
        AttachmentsFormModule,
        StatementInformationFormModule,
        SelectModule,
        StatementTableModule
    ],
    declarations: [
        StatementDetailsComponent,
        ProcessHistoryComponent,
        ProcessDiagramComponent,
        ProcessInformationComponent,
        BpmnDirective,
        StatementDetailsSideMenuComponent,
        GetProcessHistoryEntriesPipe,
        StatementDetailsAttachmentsComponent,
        StatementDetailsContributionsComponent,
        StatementDetailsGeographicPositionComponent,
        StatementDetailsInformationComponent,
        StatementDetailsLinkedStatementsComponent,
        StatementDetailsOutboxComponent,
        LinkedStatementsComponent,
        StatementDetailsConsiderationsComponent
    ],
    exports: [
        StatementDetailsComponent,
        ProcessHistoryComponent,
        ProcessDiagramComponent,
        ProcessInformationComponent,
        BpmnDirective,
        StatementDetailsSideMenuComponent,
        GetProcessHistoryEntriesPipe,
        StatementDetailsAttachmentsComponent,
        StatementDetailsContributionsComponent,
        StatementDetailsGeographicPositionComponent,
        StatementDetailsInformationComponent,
        StatementDetailsLinkedStatementsComponent,
        StatementDetailsOutboxComponent,
        LinkedStatementsComponent,
        StatementDetailsConsiderationsComponent
    ]
})
export class StatementDetailsModule {

}
