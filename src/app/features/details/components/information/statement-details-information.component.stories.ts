/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {provideMockStore} from "@ngrx/store/testing";
import {withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core/i18n";
import {StatementDetailsModule} from "../../statement-details.module";

storiesOf("Features / Forms / Details", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        providers: [
            provideMockStore()
        ],
        imports: [
            I18nModule,
            StatementDetailsModule
        ]
    }))
    .add("StatementDetailsInformationComponent", () => ({
        template: `
            <app-statement-details-information>
            </app-statement-details-information>
        `,
        props: {}
    }));
