/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {take} from "rxjs/operators";
import {openGisAction, statementGeographicPositionSelector, userNameSelector} from "../../../../store";
import {ILeafletBounds} from "../../../map";

/**
 * This component displays the statements coordinates on a map using leaflet.
 * No editing of the set coordinates is possible.
 */

@Component({
    selector: "app-statement-details-geographic-position",
    templateUrl: "./statement-details-geographic-position.component.html",
    styleUrls: ["./statement-details-geographic-position.component.scss"]
})
export class StatementDetailsGeographicPositionComponent {

    @Input()
    public appCollapsed = false;

    public geographicPosition$ = this.store.pipe(select(statementGeographicPositionSelector));

    public userName$ = this.store.pipe(select(userNameSelector));

    public constructor(public store: Store) {

    }

    public openGis(bounds: ILeafletBounds) {
        this.userName$.pipe(take(1)).subscribe((user) => {
            this.store.dispatch(openGisAction({bounds, user}));
        });
    }

}
