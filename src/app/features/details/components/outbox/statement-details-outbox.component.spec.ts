/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core/i18n";
import {startAttachmentDownloadAction} from "../../../../store/attachments/actions";
import {queryParamsIdSelector} from "../../../../store/root/selectors";
import {StatementDetailsModule} from "../../statement-details.module";
import {StatementDetailsOutboxComponent} from "./statement-details-outbox.component";

describe("StatementDetailsOutboxComponent", () => {
    let component: StatementDetailsOutboxComponent;
    let fixture: ComponentFixture<StatementDetailsOutboxComponent>;
    let store: Store;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementDetailsModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: queryParamsIdSelector,
                            value: 19
                        }
                    ]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementDetailsOutboxComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should dispatch startAttachmentDownloadAction with correct attachmentId", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.downloadAttachment(15);
        expect(dispatchSpy).toHaveBeenCalledWith(startAttachmentDownloadAction({statementId: 19, attachmentId: 15}));
    });
});
