/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed} from "@angular/core/testing";
import {I18nModule} from "../../../../../core/i18n";
import {StatementDetailsModule} from "../../../statement-details.module";
import {ProcessDiagramComponent} from "./process-diagram.component";

describe("ProcessDiagramComponent", () => {
    let component: ProcessDiagramComponent;
    let fixture: ComponentFixture<ProcessDiagramComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ProcessDiagramComponent],
            imports: [
                StatementDetailsModule,
                I18nModule,
            ],
        }).compileComponents();
    });
    beforeEach(() => {
        fixture = TestBed.createComponent(ProcessDiagramComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
