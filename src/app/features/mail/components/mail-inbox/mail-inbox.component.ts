/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIEmailModel} from "../../../../core/api/mail";
import {momentFormatDisplayNumeric} from "../../../../util/moment";

@Component({
    selector: "app-mail-inbox",
    templateUrl: "./mail-inbox.component.html",
    styleUrls: ["./mail-inbox.component.scss"]
})
export class MailInboxComponent {

    @Input()
    public appMails: IAPIEmailModel[];

    @Input()
    public appLoading: boolean;

    @Input()
    public appSelectedMailId: string;

    @Output()
    public appFetch = new EventEmitter();

    public dateFormat = momentFormatDisplayNumeric;

}
