/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";

/**
 * Removes all new lines (\n( from the input string and returns an array split by new lines.
 */

@Pipe({
    name: "appEmailTextToArray"
})
export class EmailTextToArrayPipe implements PipeTransform {

    public transform(text: string): Array<string> {
        const textAsArray = text.replace("\r", "").split("\n");
        return textAsArray.filter((el, index) => el.trim() !== "" || (index > 0 && index < textAsArray.length - 1));
    }

}
