/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {Observable, Subject} from "rxjs";
import {filter, take, takeUntil} from "rxjs/operators";
import {IStoreTextblockHistoryVersionModel} from "../../../../core/api/statements/IAPITextblockHistoryVersionModel";
import {ISelectOption} from "../../../../shared/controls/select";
import {
    fetchStatementTextblockHistoryAction,
    IStatementEntity,
    queryParamsIdSelector,
    statementLoadingSelector,
    statementSelector,
    statementTitleSelector
} from "../../../../store";
import {
    statementTextblockHistorySelector,
    statementTextblockHistoryVersionsSelector
} from "../../../../store/statements/selectors/textblock-history/textblock-history.selectors";
import {ITextblockHistoryVersionModel} from "../../interfaces/ITextblockHistoryVersionModel";

@Component({
    selector: "app-statement-details",
    templateUrl: "./history.component.html",
    styleUrls: ["./history.component.scss"]
})
export class HistoryComponent implements OnInit, OnDestroy {

    public statement$: Observable<IStatementEntity> = this.store.pipe(select(statementSelector));

    public title$ = this.store.pipe(select(statementTitleSelector));

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public history$ = this.store.pipe(select(statementTextblockHistorySelector));

    public versions$ = this.store.pipe(select(statementTextblockHistoryVersionsSelector));

    public isStatementLoading$ = this.store.pipe(select(statementLoadingSelector));

    public selectedVersion: ITextblockHistoryVersionModel;

    public selectedOption: string;

    public lastVersion: IStoreTextblockHistoryVersionModel;

    public selectedMode: ISelectOption<string>;

    public appModeOptions: ISelectOption<string>[] = [
        {label: "normal", value: "normal"},
        {label: "details", value: "details"}
    ];

    private destroy$ = new Subject<void>();

    private initializing = true;

    public constructor(public readonly store: Store) {
        this.selectedMode = this.appModeOptions.find(_ => true);
    }

    public ngOnInit(): void {
        this.statementId$
            .pipe(
                filter((statementId) => statementId != null),
                takeUntil(this.destroy$)
            )
            .subscribe((statementId) => {
                this.store.dispatch(fetchStatementTextblockHistoryAction({statementId}));
            });

        this.history$
            .pipe(
                filter((history) => history != null),
                takeUntil(this.destroy$)
            )
            .subscribe((history) => {
                if (history?.versions != null && history.versionOrder?.length > 0) {
                    this.lastVersion = history.versions[history.versionOrder[history.versionOrder.length - 1]];
                }
                if (this.initializing && this.lastVersion?.version != null) {
                    this.selectVersion(this.lastVersion.version);
                    this.initializing = false;
                }
            });
    }

    public async ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public async selectVersion(event: any) {
        if (event == null) {
            return;
        }
        const history = await this.history$.pipe(take(1)).toPromise();
        if (history?.versions != null && Object.keys(history.versions).length > 0) {
            const versionToSelect = history.versions[event];

            this.selectedVersion = {
                ...versionToSelect,
                content: versionToSelect?.content != null ? versionToSelect.content.map((contentElement) => {
                    const previousVersion = history.versions[contentElement.prevVersion];
                    return {
                        ...contentElement,
                        version: versionToSelect?.version,
                        timestamp: versionToSelect?.timestamp,
                        user: versionToSelect?.user,
                        firstName: versionToSelect?.firstName,
                        lastName: versionToSelect?.lastName,
                        prevVersion: {
                            version: previousVersion?.version,
                            timestamp: previousVersion?.timestamp,
                            user: previousVersion?.user,
                            firstName: previousVersion?.firstName,
                            lastName: previousVersion?.lastName
                        }
                    };
                }) : []
            };
            this.selectedOption = event;
        }
    }

    public changeMode() {
        this.selectedMode = this.appModeOptions.find(_ => _.label !== this.selectedMode.label);
    }

    public async selectVersionFromIndex(event: number) {
        const versions = await this.versions$.pipe(take(1)).toPromise();
        if (this.selectedOption == null || versions?.length <= 0) {
            return;
        }
        const currentVersion = versions.find((version) => version.value === this.selectedOption);
        const currentVersionIndex = versions.indexOf(currentVersion);
        const newIndex = currentVersionIndex + event;
        if (newIndex >= versions.length || newIndex < 0) {
            return;
        }
        await this.selectVersion(versions[newIndex].value);
    }
}
