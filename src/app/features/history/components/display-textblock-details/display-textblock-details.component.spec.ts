/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {I18nModule} from "../../../../core";
import {HistoryModule} from "../../history.module";
import {DisplayTextblockDetailsComponent} from "./display-textblock-details.component";

describe("DisplayTextblockDetailsComponent", () => {
    let component: DisplayTextblockDetailsComponent;
    let fixture: ComponentFixture<DisplayTextblockDetailsComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                HistoryModule
            ],
            declarations: [DisplayTextblockDetailsComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DisplayTextblockDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
