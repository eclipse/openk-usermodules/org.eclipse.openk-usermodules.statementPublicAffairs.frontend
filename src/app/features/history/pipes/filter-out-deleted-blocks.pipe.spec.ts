/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {ITextblockHistoryVersionContentModel} from "../interfaces/ITextblockHistoryVersionContentModel";
import {FilterOutDeletedBlocksPipe} from "./filter-out-deleted-blocks.pipe";

describe("FilterOutDeletedBlocksPipe", () => {

    const pipe = new FilterOutDeletedBlocksPipe();

    describe("transform", () => {

        it("should return empty array for invalid input", () => {
            let result = pipe.transform(null);
            expect(result).toEqual([]);
            result = pipe.transform(undefined);
            expect(result).toEqual([]);
        });

        it("should return empty array for invalid input", () => {
            const models: ITextblockHistoryVersionContentModel[] = [
                {
                    id: "id",
                    diffType: "NEW"
                },
                {
                    id: "id2",
                    diffType: "DELETED"
                }
            ] as unknown as ITextblockHistoryVersionContentModel[];

            const result = pipe.transform(models);
            expect(result).toEqual([
                models[0]
            ]);
        });
    });
});

