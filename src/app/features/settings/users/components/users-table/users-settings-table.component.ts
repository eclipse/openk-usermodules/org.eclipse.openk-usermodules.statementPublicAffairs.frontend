/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIUserInfoExtended} from "../../../../../core";

@Component({
    selector: "app-users-settings-table",
    templateUrl: "./users-settings-table.component.html",
    styleUrls: ["./users-settings-table.component.scss"]
})
export class UsersSettingsTableComponent {

    @Input()
    public appUsers: IAPIUserInfoExtended[];

    @Input()
    public appColumns = ["user-name", "first-name", "last-name", "email", "roles"];

    @Output()
    public appSelectedUserChange = new EventEmitter<number>();

    public appSelectedUser: number = null;

    public onSelect(user: IAPIUserInfoExtended) {
        this.appSelectedUser = user.id;
        this.appSelectedUserChange.emit(this.appSelectedUser);
    }

}
