/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {submitTextblockSettingsAction} from "../../../../store";
import {TextBlockSettingsModule} from "../text-block-settings.module";
import {TextBlocksSettingsComponent} from "./text-blocks-settings.component";

describe("TextblockSettingsComponent", () => {
    let component: TextBlocksSettingsComponent;
    let fixture: ComponentFixture<TextBlocksSettingsComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                TextBlockSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlocksSettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        store = TestBed.inject(MockStore);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should track by index", () => {
        expect(component.trackByIndex(19)).toBe(19);
    });

    it("should submit data", () => {
        spyOn(store, "dispatch");
        component.submit();
        expect(store.dispatch).toHaveBeenCalledWith(submitTextblockSettingsAction({data: component.form.getValue()}));
    });

});
