/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ConnectedPosition} from "@angular/cdk/overlay";
import {Component, EventEmitter, Output} from "@angular/core";

@Component({
    selector: "app-nav-drop-down",
    templateUrl: "./nav-drop-down.component.html",
    styleUrls: ["./nav-drop-down.component.scss"]
})
export class NavDropDownComponent {

    public isOpen: boolean;

    public connectedPositions: ConnectedPosition[] = [
        {
            originX: "center",
            originY: "bottom",
            overlayX: "center",
            overlayY: "top",
            panelClass: "bottom"
        }
    ];

    @Output()
    public appLogOut = new EventEmitter<void>();

    public logOut() {
        this.appLogOut.emit();
    }

}
