/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPISectorsModel} from "../../../../core/api/statements/IAPISectorsModel";

/**
 * For given city and district, returns the available sectors.
 */

@Pipe({
    name: "sector"
})
export class SectorPipe implements PipeTransform {
    transform(sectors: IAPISectorsModel, city: string, district: string): any {

        if (sectors && city && district) {
            return sectors[city + "#" + district]?.map((_) => " " + _).toString();
        } else {
            return undefined;
        }
    }
}
