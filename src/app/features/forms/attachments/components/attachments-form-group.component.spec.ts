/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {clearAttachmentCacheAction, queryParamsIdSelector, startAttachmentDownloadAction} from "../../../../store";
import {AttachmentsFormModule} from "../attachments-form.module";
import {AttachmentsFormGroupComponent} from "./attachments-form-group.component";

describe("AttachmentsFormGroupComponent", () => {

    const statementId = 19;

    let storeMock: MockStore;
    let component: AttachmentsFormGroupComponent;
    let fixture: ComponentFixture<AttachmentsFormGroupComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                AttachmentsFormModule
            ],
            providers: [
                provideMockStore({
                    initialState: {statements: {}, settings: {}, contacts: {}, attachments: {}},
                    selectors: [
                        {
                            selector: queryParamsIdSelector,
                            value: statementId
                        }
                    ]
                })
            ]
        }).compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AttachmentsFormGroupComponent);
        component = fixture.componentInstance;
        storeMock = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should clear file cache on destruction", async () => {
        const dispatchSpy = spyOn(storeMock, "dispatch").and.callThrough();
        component.ngOnDestroy();
        await fixture.whenStable();
        expect(dispatchSpy).toHaveBeenCalledWith(clearAttachmentCacheAction({statementId}));
    });

    it("should download attachments", async () => {
        const dispatchSpy = spyOn(storeMock, "dispatch");
        const attachmentId = 1919;
        await component.downloadAttachment(attachmentId);
        expect(dispatchSpy).toHaveBeenCalledWith(startAttachmentDownloadAction({statementId, attachmentId}));
    });

});
