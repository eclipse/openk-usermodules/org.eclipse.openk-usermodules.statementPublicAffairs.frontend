/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormArray, FormControl} from "@angular/forms";
import {text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core";
import {IAttachmentControlValue, IAttachmentFormValue} from "../../../../../store";
import {createAttachmentTagList} from "../../../../../test";
import {createFormGroup} from "../../../../../util/forms";
import {AttachmentsFormModule} from "../../attachments-form.module";

function createAttachment(name: string, id: number): IAttachmentControlValue {
    return {
        name,
        id,
        tagIds: ["" + Math.floor(5 * Math.random())],
        isSelected: true
    };
}

const appFormGroup = createFormGroup<IAttachmentFormValue>({
    add: new FormArray([]),
    edit: new FormArray([
        "Übersicht.pdf", "Anhang.pdf", "Bauplan.pdf"
    ].map((_, i) => new FormControl(createAttachment(_, i))))
});

storiesOf("Features / Forms / Attachments", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            AttachmentsFormModule
        ]
    }))
    .add("AttachmentListFormComponent", () => ({
        template: `
            <app-attachment-list-form style="margin: 1em; box-sizing: border-box"
                [appFormGroup]="appFormGroup"
                [appTagList]="appTagList"
                [appTitle]="appTitle()"
            >
            </app-attachment-list-form>
        `,
        props: {
            appTitle: () => text("appTitle", "Anhänge übernehmen"),
            appFormGroup,
            appTagList: createAttachmentTagList()
        }
    }));
