/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIAttachmentTag} from "../../../../../core/api/attachments";
import {IAttachmentControlValue} from "../../../../../store/attachments/model";

@Component({
    selector: "app-attachment-display-list",
    templateUrl: "./attachment-display-list.component.html",
    styleUrls: ["./attachment-display-list.component.scss"]
})
export class AttachmentDisplayListComponent {

    @Input()
    public appTitle: string;

    @Input()
    public appAttachments: IAttachmentControlValue[] = [];

    @Input()
    public appTagList: IAPIAttachmentTag[] = [];

    @Output()
    public appDownload = new EventEmitter<number>();

}
