/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {IAPITextArrangementItemModel, IAPITextBlockModel} from "../../../../../core/api/text";
import {I18nModule} from "../../../../../core/i18n";
import {StatementEditorModule} from "../../statement-editor.module";
import {TextBlockControlComponent} from "./text-block-control.component";

describe("TextBlockControlComponent", () => {
    let component: TextBlockControlComponent;
    let fixture: ComponentFixture<TextBlockControlComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementEditorModule,
                I18nModule
            ],
            declarations: [TextBlockControlComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlockControlComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appAdd with a newline block", () => {
        spyOn(component.appAdd, "emit").and.callThrough();
        component.addNewLine();
        expect(component.appAdd.emit).toHaveBeenCalledWith({type: "newline", placeholderValues: {}});
    });

    it("should set new value for the placeholder", () => {
        const replacement: { name: string; newValue: string } = {
            name: "name",
            newValue: "newValue"
        };
        component.addPlaceholder(replacement);
        expect(component.appValue.placeholderValues).toEqual({name: "newValue"});

        component.addPlaceholder({...replacement, newValue: "value"});
        expect(component.appValue.placeholderValues).toEqual({name: "value"});
    });

    it("should keep replacement text if there is one", () => {
        const value: IAPITextArrangementItemModel = {type: "block", placeholderValues: {}};
        component.appValue = value;
        component.convertToFreeText();
        expect(component.appValue).toEqual({...value, replacement: ""});

        const replacement = "replacement";
        component.appValue.replacement = replacement;
        component.convertToFreeText();
        expect(component.appValue).toEqual({...value, replacement});

        component.appValue.replacement = undefined;
        const textBlockModel: IAPITextBlockModel = {id: "1", text: "text \n\nblock text", excludes: [], requires: []};
        component.appTextBlockModel = textBlockModel;
        component.convertToFreeText();
        expect(component.appValue).toEqual({...value, replacement: textBlockModel.text});
    });

    it("should keep replacement text if there is one", () => {
        component.appValue = undefined;
        const replacement = "replacement";
        component.setReplacement(replacement);
        expect(component.appValue).toEqual({replacement} as IAPITextArrangementItemModel);
    });

});
