/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Inject, OnDestroy, OnInit} from "@angular/core";
import {FormArray} from "@angular/forms";
import {select, Store} from "@ngrx/store";
import {combineLatest, defer, Observable} from "rxjs";
import {filter, map, skip, switchMap, take, takeUntil} from "rxjs/operators";
import {
    APP_CONFIGURATION,
    EAPIProcessTaskDefinitionKey,
    EAPIStaticAttachmentTagIds,
    IAPITextArrangementErrorModel,
    IAppConfiguration,
    TCompleteTaskVariable
} from "../../../../../core";
import {
    compileStatementArrangementAction,
    createStatementEditorForm,
    EErrorCode,
    fetchStatementTextArrangementAction,
    getContributionsSelector,
    getStatementArrangementErrorSelector,
    getStatementArrangementForCurrentTaskSelector,
    getStatementEditorControlConfigurationSelector,
    getStatementErrorSelector,
    getStatementStaticTextReplacementsSelector,
    getStatementTextBlockGroupsForCurrentTaskSelector,
    IStatementEditorFormValue,
    IStatementEntity,
    IStatementErrorEntity,
    queryParamsIdSelector,
    requiredContributionsGroupsSelector,
    requiredContributionsOptionsSelector,
    setErrorAction,
    statementFileSelector,
    statementSelector,
    submitStatementEditorFormAction,
    updateStatementEntityAction,
    userRolesSelector,
    validateStatementArrangementAction
} from "../../../../../store";
import {arrayJoin, filterDistinctValues} from "../../../../../util";
import {AbstractEditorFormDirective} from "../../../abstract/abstract-editor-form.directive";

/**
 * This component displays information about the statement.
 * Also gives the possibility to upload attachments to send as response. (outbox attachments)
 * The component is shown for different task states.
 * For the official in charge, when creating or checking the response, the current state of contributions is also shown and can be edited.
 * The text content for the response can be edited by placing and filling the text blocks.
 */

@Component({
    selector: "app-statement-editor-form",
    templateUrl: "./statement-editor-form.component.html",
    styleUrls: ["./statement-editor-form.component.scss"]
})
export class StatementEditorFormComponent extends AbstractEditorFormDirective<IStatementEditorFormValue> implements OnInit, OnDestroy {

    public outboxTagId = EAPIStaticAttachmentTagIds.OUTBOX;

    public statementTagId = EAPIStaticAttachmentTagIds.STATEMENT;

    public appShortMode: boolean;

    public appShowPreview: boolean;

    public appFormGroup = createStatementEditorForm();

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public statement$: Observable<IStatementEntity> = this.store.pipe(select(statementSelector));

    public showContributions$ = defer(() => this.task$).pipe(
        map((task) => task?.taskDefinitionKey),
        map((taskDefinitionKey) => taskDefinitionKey !== EAPIProcessTaskDefinitionKey.CREATE_NEGATIVE_RESPONSE)
    );

    public showContributionsControl$ = defer(() => this.task$).pipe(
        map((task) => task?.taskDefinitionKey),
        map((taskDefinitionKey) => taskDefinitionKey === EAPIProcessTaskDefinitionKey.CREATE_DRAFT
                || taskDefinitionKey === EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE)
    );

    public requiredContributionOptions$ = this.store.pipe(select(requiredContributionsOptionsSelector));

    public requiredContributionGroups$ = this.store.pipe(select(requiredContributionsGroupsSelector));

    public contributions$ = this.store.pipe(select(getContributionsSelector));

    public selectedContributionsCount$ = this.value$.pipe(
        map((value) => value?.contributions?.selected?.length)
    );

    public userRoles$ = this.store.pipe(select(userRolesSelector));

    public controls$ = this.value$.pipe(
        filter((value) => value != null),
        switchMap((value) => this.store.pipe(select(getStatementEditorControlConfigurationSelector, arrayJoin(value.arrangement))))
    );

    public replacements$ = this.store.pipe(select(getStatementStaticTextReplacementsSelector));

    public selectedTextBlockIds$: Observable<string[]> = this.value$.pipe(
        map((value) => filterDistinctValues(value.arrangement.map((item) => item.textblockId)))
    );

    public textBlockGroups$ = this.store.pipe(select(getStatementTextBlockGroupsForCurrentTaskSelector));

    public arrangement$ = this.store.pipe(select(getStatementArrangementForCurrentTaskSelector));

    public file$ = this.store.pipe(select(statementFileSelector));

    public arrangementError$ = this.store.pipe(select(getStatementArrangementErrorSelector));

    public error$: Observable<IStatementErrorEntity> = this.store.pipe(select(getStatementErrorSelector));

    public constructor(
        public store: Store,
        @Inject(APP_CONFIGURATION) public configuration: IAppConfiguration
    ) {
        super(store, configuration);
    }

    public async ngOnInit() {
        this.updateForm();
        this.fetchTextArrangement();
        this.deleteStatementFile();
        if (this.configuration.claimDetails?.pollClaimDetails) {
            await this.readClaimDetails();
        }
        this.initializing = false;
    }

    public ngOnDestroy() {
        super.ngOnDestroy();
        this.initializing = true;
        this.unsubscribeClaimDetails$.next();
        this.unsubscribeClaimDetails$.complete();
        this.deleteStatementFile();
        this.clearErrors();
    }

    public setArrangementErrors(errors: IAPITextArrangementErrorModel[]) {
        const array = this.appFormGroup.get("arrangement");
        if (array instanceof FormArray) {
            array.controls
                .forEach((control) => control.setErrors({arrangement: null}));
            arrayJoin(errors)
                .forEach((e) => array.get([e?.arrangementId])?.setErrors({arrangement: e}));
        }
    }

    public async validate() {
        this.clearErrors();
        const task = await this.task$.pipe(take(1)).toPromise();
        this.store.dispatch(validateStatementArrangementAction({
            statementId: task.statementId,
            taskId: task.taskId,
            arrangement: this.getValue().arrangement
        }));
    }

    public async compile() {
        this.clearErrors();
        const task = await this.task$.pipe(take(1)).toPromise();
        this.store.dispatch(compileStatementArrangementAction({
            statementId: task.statementId,
            taskId: task.taskId,
            arrangement: this.getValue().arrangement
        }));
    }

    public async submit(options?: {
        completeTask?: TCompleteTaskVariable;
        claimNext?: boolean | EAPIProcessTaskDefinitionKey;
        compile?: boolean;
        contribute?: boolean;
        file?: File;
    }) {
        this.clearErrors();
        const task = await this.task$.pipe(take(1)).toPromise();
        const claimDetails = await this.claimDetails$.pipe(take(1)).toPromise();
        const value = this.getValue();
        this.store.dispatch(submitStatementEditorFormAction({
            statementId: task.statementId,
            taskId: task.taskId,
            value: {
                ...value,
                contributions: await this.showContributionsControl$.pipe(take(1)).toPromise() ? value.contributions : null
            },
            options,
            customError: this.configuration.claimDetails?.pollClaimDetails && claimDetails[task.taskId] == null
                ? EErrorCode.TASK_NOT_CLAIMED_BY_USER : null
        }));
        if (this.configuration.claimDetails?.pollClaimDetails && options == null && !this.claimLost) {
            this.isStatementLoading$.pipe(
                filter((loading) => !loading),
                take(1)
            ).subscribe(() => {
                this.fetchClaimDetails();
            });
        }
    }

    public async finalize(complete?: boolean) {
        this.clearErrors();
        if (complete) {
            const file = await this.file$.pipe(take(1)).toPromise();
            return this.submit({
                completeTask: {
                    data_complete: {type: "Boolean", value: true},
                    response_created: {type: "Boolean", value: true}
                },
                file
            });
        } else {
            return this.deleteStatementFile();
        }
    }

    private deleteStatementFile() {
        combineLatest([this.statementId$, this.file$]).pipe(
            take(1),
            filter(([statementId, file]) => statementId != null && file != null)
        ).subscribe(([statementId]) => {
            this.store.dispatch(updateStatementEntityAction({statementId, entity: {file: null}}));
        });
    }

    private fetchTextArrangement() {
        this.task$.pipe(filter((task) => task != null), takeUntil(this.destroy$))
            .subscribe(({statementId}) => this.store.dispatch(fetchStatementTextArrangementAction({statementId})));
    }

    private updateForm() {
        this.isStatementLoading$.pipe(takeUntil(this.destroy$))
            .subscribe((loading) => this.disable(loading));
        this.arrangement$.pipe(takeUntil(this.destroy$))
            .subscribe((arrangement) => this.setValueForArray(arrangement, "arrangement"));
        this.contributions$.pipe(takeUntil(this.destroy$))
            .subscribe((contributions) => this.patchValue({contributions}));
        this.arrangementError$.pipe(
            skip(1), // The first value is skipped when the user enters the site.
            switchMap((errors) =>
                // Errors are only displayed when the form is ready to use.
                this.appFormGroup.statusChanges.pipe(
                    filter(() => this.appFormGroup.enabled),
                    map(() => errors),
                    take(1)
                )
            ),
            takeUntil(this.destroy$),
        ).subscribe((errors) => this.setArrangementErrors(errors));
    }

    private clearErrors() {
        combineLatest([this.task$, this.isStatementLoading$]).pipe(
            take(1),
            filter(([task, loading]) => task?.statementId != null && !loading)
        ).subscribe(([task]) => {
            this.store.dispatch(setErrorAction({
                statementId: task.statementId,
                error: null
            }));
        });
    }
}
