/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {ClaimDetailsModule} from "../../../shared/controls/claim-details";
import {SelectModule} from "../../../shared/controls/select";
import {StatementSelectModule} from "../../../shared/controls/statement-select";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {StatementDetailsModule} from "../../details";
import {MapModule} from "../../map";
import {WorkflowDataFormComponent, WorkflowDataSideMenuComponent} from "./components";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatIconModule,
        TranslateModule,
        ClaimDetailsModule,
        CollapsibleModule,
        SelectModule,
        StatementSelectModule,
        SideMenuModule,
        ActionButtonModule,
        MapModule,
        StatementDetailsModule
    ],
    declarations: [
        WorkflowDataFormComponent,
        WorkflowDataSideMenuComponent
    ],
    exports: [
        WorkflowDataFormComponent,
        WorkflowDataSideMenuComponent
    ]
})
export class WorkflowDataFormModule {

}
