/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Inject, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {combineLatest, Observable, Subscription} from "rxjs";
import {distinctUntilChanged, filter, take, takeUntil} from "rxjs/operators";
import {APP_CONFIGURATION, IAPIProcessTask, IAPISearchOptions, IAppConfiguration} from "../../../../core";
import {
    createWorkflowForm,
    deleteClaimDetailsAction,
    departmentGroupsSelector,
    departmentOptionsSelector,
    EErrorCode,
    fetchStatementDetailsAction,
    getSearchContentStatementsSelector,
    getStatementErrorSelector,
    IStatementEntity,
    IStatementErrorEntity,
    IWorkflowFormValue,
    openGisAction,
    queryParamsIdSelector,
    setErrorAction,
    startStatementSearchAction,
    statementGeographicPositionSelector,
    statementSelector,
    statementTypeOptionsSelector,
    submitWorkflowDataFormAction,
    workflowFormValueSelector
} from "../../../../store";
import {ILeafletBounds, latLngZoomToString} from "../../../map";
import {AbstractEditorFormDirective} from "../../abstract/abstract-editor-form.directive";

/**
 * This component displays all the workflow information for the statement. (departments/geo-coordinates/linked statements)
 * The coordinates can be set by selecting a point on a integrated map. Previous statements can be selected from a list to link them
 * together. Departments from whom input is needed, can be made mandatory or optional.
 */
@Component({
    selector: "app-workflow-data-form",
    templateUrl: "./workflow-data-form.component.html",
    styleUrls: ["./workflow-data-form.component.scss"]
})
export class WorkflowDataFormComponent
    extends AbstractEditorFormDirective<IWorkflowFormValue>
    implements OnInit, OnDestroy {

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public statementTypes$ = this.store.pipe(select(statementTypeOptionsSelector));

    public searchContent$ = this.store.pipe(select(getSearchContentStatementsSelector));

    public departmentOptions$ = this.store.pipe(select(departmentOptionsSelector));

    public departmentGroups$ = this.store.pipe(select(departmentGroupsSelector));

    public appFormGroup = createWorkflowForm();

    public appErrorMessage$: Observable<IStatementErrorEntity> = this.store.pipe(select(getStatementErrorSelector));

    public statement$: Observable<IStatementEntity> = this.store.pipe(select(statementSelector));

    public subscription: Subscription;

    public defaultGeographicPosition = latLngZoomToString(this.configuration.leaflet, this.configuration.leaflet.zoom);

    public geographicPosition$ = this.store.pipe(select(statementGeographicPositionSelector));

    private form$ = this.store.pipe(select(workflowFormValueSelector));

    public constructor(
        public store: Store,
        @Inject(APP_CONFIGURATION) public configuration: IAppConfiguration
    ) {
        super(store, configuration);
    }

    public async ngOnInit() {
        this.updateForm();
        this.task$.pipe(takeUntil(this.destroy$)).subscribe(() => this.search({q: ""}));
        this.subscription = this.statementId$
            .pipe(filter((statementId) => statementId != null))
            .subscribe((statementId) => this.store.dispatch(fetchStatementDetailsAction({statementId})));
        if (this.configuration.claimDetails?.pollClaimDetails) {
            await this.readClaimDetails();
        }
        this.initializing = false;
    }

    public ngOnDestroy() {
        super.ngOnDestroy();
        this.initializing = true;
        this.unsubscribeClaimDetails$.next();
        this.unsubscribeClaimDetails$.complete();
        if (this.subscription != null) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
        return this.clearErrors();
    }

    public async submit(completeTask?: boolean) {
        this.clearErrors();
        const task = await this.task$.pipe(take(1)).toPromise();
        const claimDetails = await this.claimDetails$.pipe(take(1)).toPromise();
        this.store.dispatch(submitWorkflowDataFormAction({
            statementId: task.statementId,
            taskId: task.taskId,
            data: this.getValue(),
            completeTask,
            customError: this.configuration.claimDetails?.pollClaimDetails && claimDetails[task.taskId] == null
                ? EErrorCode.TASK_NOT_CLAIMED_BY_USER : null
        }));
        if (this.configuration.claimDetails?.pollClaimDetails && completeTask !== null && !this.claimLost) {
            this.isStatementLoading$.pipe(
                filter((loading) => !loading),
                take(1)
            ).subscribe(() => {
                this.fetchClaimDetails();
            });
        }
    }

    public search(options: IAPISearchOptions) {
        this.store.dispatch(startStatementSearchAction({options}));
    }

    public openGis(bounds: ILeafletBounds) {
        this.userName$.pipe(take(1)).subscribe((user) => {
            this.store.dispatch(openGisAction({bounds, user}));
        });
    }

    public clearErrors() {
        combineLatest([this.task$, this.isStatementLoading$]).pipe(
            take(1),
            filter(([task, loading]) => task?.statementId != null && !loading)
        ).subscribe(([task]) => {
            this.store.dispatch(setErrorAction({
                statementId: task.statementId,
                error: null
            }));
        });
    }

    public setErrorAndResetClaimDetails(task: IAPIProcessTask) {
        this.unsubscribeClaimDetails$.next();
        this.store.dispatch(setErrorAction({
            error: EErrorCode.TASK_NOT_CLAIMED_BY_USER,
            statementId: task.statementId,
            setBothErrors: true
        }));
        this.store.dispatch(deleteClaimDetailsAction({
            taskId: task.taskId
        }));
        this.claimLost = true;
    }

    private updateForm() {
        this.isStatementLoading$.pipe(takeUntil(this.destroy$), distinctUntilChanged())
            .subscribe((loading) => loading ? this.appFormGroup.disable() : this.appFormGroup.enable());
        this.form$.pipe(takeUntil(this.destroy$)).subscribe((value) => {
            const geographicPosition = value.geographicPosition == null ? this.defaultGeographicPosition : value.geographicPosition;
            this.patchValue({...value, geographicPosition});
        });
    }
}
