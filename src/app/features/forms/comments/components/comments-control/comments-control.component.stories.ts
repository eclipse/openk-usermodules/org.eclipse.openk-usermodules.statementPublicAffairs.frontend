/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {IAPICommentModel} from "../../../../../core/api/statements";
import {I18nModule} from "../../../../../core/i18n";
import {CommentsFormModule} from "../../comments-form.module";
import {CommentsControlComponent} from "./comments-control.component";

const comments: IAPICommentModel[] = [
    {
        id: 0,
        text: "Ein kurzer Kommentar.",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2015-08-31T16:47+00:00",
        editable: true
    },
    {
        id: 1,
        text: "Ein längerer Kommentar. Ein weiterer Satz. Ein weiterer Satz. Ein weiterer Satz. Ein weiterer Satz. Ein weiterer Satz.",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2015-08-31T18:13+00:00",
        editable: true
    },
    {
        id: 2,
        text: "Ein Kommentar von einem anderen Nutzer mit einem Zeilenumbruch. \nWeitere Zeile.",
        userName: "User2",
        firstName: "Peter",
        lastName: "Fox",
        timestamp: "2015-09-01T06:17+00:00",
        editable: false
    },
    {
        id: 3,
        text: "Ein weiterer Kommentar von einem anderen Nutzer.",
        userName: "User2",
        firstName: "Peter",
        lastName: "Fox",
        timestamp: "2015-09-01T11:17+00:00",
        editable: false
    },
    {
        id: 4,
        text: "a",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2015-09-01T11:17+00:00",
        editable: true
    },
    {
        id: 5,
        text: `Extrem langer Kommentar. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz.
            Das ist ein Satz. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz.
            Das ist ein Satz. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz.
            Das ist ein Satz. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz. Das ist ein Satz.
            Das ist ein Satz. Das ist ein Satz.`,
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2015-09-01T11:17+00:00",
        editable: true
    },
    {
        id: 6,
        text: "test text",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2007-08-31T16:47+00:00",
        editable: true
    },
    {
        id: 7,
        text: "test text",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2007-08-31T16:47+00:00",
        editable: true
    },
    {
        id: 8,
        text: "test text",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2007-08-31T16:47+00:00",
        editable: true
    },
    {
        id: 9,
        text: "test text",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2007-08-31T16:47+00:00",
        editable: true
    },
    {
        id: 10,
        text: "test text",
        userName: "User1",
        firstName: "Franz",
        lastName: "Meier",
        timestamp: "2007-08-31T16:47+00:00",
        editable: true
    }
].map((comment, id) => ({...comment, text: id + " " + comment.text}));

const addComment = (text: string) => {
    comments.push(
        {
            id: comments.length,
            text: comments.length + " " + text,
            userName: "test01",
            firstName: "Vorname",
            lastName: "Nachname",
            timestamp: new Date().toString(),
            editable: true
        }
    );
};

const deleteComment = (id: number) => {
    comments.splice(id, 1);
    for (let i = id; i < comments.length; i++) {
        comments[i].id--;
    }
};

storiesOf("Features / Forms", module)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            RouterTestingModule,
            CommentsFormModule
        ]
    }))
    .add("CommentsControlComponent", () => ({
        template: `
            <app-comments-control style="padding: 1em; box-sizing: border-box"
                [appComments]="comments"
                (appAdd)="addComment($event)"
                (appDelete)="deleteComment($event)">
            </app-comments-control>
        `,
        props: {
            comments,
            addComment,
            deleteComment
        }
    }));


